module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '3f0b4428350e57f5d34197d9a95a0dd5'),
    },
  },
  url: env('URL')
});
