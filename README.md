<p align="center" class="text-center">
  <a href="https://digitales-wartezimmer.org/" target="blank"><img src="https://i.imgur.com/z4zeQQO.png" width="320" alt="Digitales Wartezimmer Logo" /></a>
</p>
  
<p align="center" class="text-center">A digital solution for simple and efficient exchange of information between health authorities and citizens for rapid containment of infectious diseases.</p>
<p align="center" class="text-center">
<a href="https://gitlab.com/digitales-wartezimmer/content-backend/-/pipelines" target="_blank"><img alt="Gitlab pipeline status" src="https://img.shields.io/gitlab/pipeline/digitales-wartezimmer/content-backend/main?label=build"></a>
<a href="https://status.digitales-wartezimmer.org" target="_blank"><img alt="Uptime Robot status Production" src="https://img.shields.io/uptimerobot/status/m786472142-8c36a3ee380d9396067b4633?label=status"></a>
<a href="https://www.gnu.org/licenses/gpl-3.0" target="_blank"><img src="https://img.shields.io/badge/License-GPLv3-brightgreen.svg" alt="License GPLv3"></a>
</p>

> :warning: This project is sadly not active anymore and unmaintained. For more info visit https://digitales-wartezimmer.org. Should you have questions regarding the codebase feel free to send an [E-mail](mailto:digitales-wartezimmer@bernhardwittmann.com).

# Digitales Wartezimmer - Content Backend

### :rocket: [https://content.digitales-wartezimmer.org](https://content.digitales-wartezimmer.org)

With our digital solution, all relevant information of COVID-19 contact persons will be digitally captured and transferred to the systems used by the responsible health authorities to track contact persons.

By digitizing the transmission of COVID-19 contact person data, we want to help health authorities break the chain of infection quickly. At the same time, the COVID-19 contact person solution should provide a fast and user-friendly way to contact his/her health authority. 

This repository is an internal tool which provides the single point of truth for all the content in Digitales Wartezimmer. This tool is based on a simple [Strapi](https://strapi.io/) CMS.

## Installation

You should have [node](https://nodejs.org/en/) and npm or [yarn](https://yarnpkg.com) installed.

```bash
$ npm install
```

## Running the app

For more Information see [Strapi Docs](https://strapi.io/documentation/).

```bash
# development
$ npm run develop

# production mode
$ npm run build && npm run start
```

## Support

Digitales Wartezimmer is a GPLv3 open source project. It is powered by our awesome [Team](https://digitales-wartezimmer.org/project). If you'd like to join us, please contact [info@digitales-wartezimmer.org](mailto:digitales-wartezimmer.org)

## Stay in touch

- Website - [https://digitales-wartezimmer.org](https://digitales-wartezimmer.org)
- Twitter - [@Dig_Wartezimmer](https://twitter.com/Dig_Wartezimmer)
- Facebook - [DigitalesWartezimmer](https://www.facebook.com/DigitalesWartezimmer/)
- LinkedIn - [digitales-wartezimmer](https://www.linkedin.com/company/digitales-wartezimmer/)

## License

  Digitales Wartezimmer is [GPLv3 licensed](LICENSE).
